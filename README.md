# Reversi-Qt

## Description

The well-known game of Reversi (a.k.a. Othello), built on C++ and Qt.

## Requirements

- C++17
- GCC >= 9.0
- Qt >= 5.12
- CMake >= 3.16

## Instructions

1. Create the build directory

```
mkdir build
cd build
```
2. Run the build

```
cmake ..
make
```

The output binary will be placed in the **build/src** directory.

# Screenshots

<img src="screenshots/01.png" width="800" alt="Reversi" />
