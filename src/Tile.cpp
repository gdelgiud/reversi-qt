#include "Tile.hpp"

Tile::Tile()
{
    is_empty = true;
}

Tile::Tile(PlayerColor playerColor)
{
    this->playerColor = playerColor;
    is_empty = false;
}

Tile Tile::empty()
{
    return Tile();
}

Tile Tile::with(PlayerColor playerColor)
{
    return Tile(playerColor);
}

bool Tile::isEmpty()
{
    return is_empty;
}

PlayerColor Tile::getColor()
{
    return playerColor;
}
