#pragma once

#include <set>
#include <optional>

#include "Tile.hpp"
#include "Position.hpp"

class Board {

public:

    static Board emptyBoard();
    static Board defaultBoard();

    Tile getTile(Position position);
    PlayerColor getCurrentPlayer();
    bool isFinished();
    bool isEmpty();
    int getScore(PlayerColor color);

    bool isValidPosition(Position position);
    std::set<Position> getAvailableMoves();
    std::set<Position> getAvailableMoves(PlayerColor color);

    std::optional<Board> applyMove(Position position);
    Board pass();

private:

    Board();
    Board(Tile *tiles, PlayerColor currentPlayer);

    void setTile(Position position, PlayerColor color);

    Tile tiles[64];
    PlayerColor currentPlayer = light;

};
