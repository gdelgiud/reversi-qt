#ifndef MINIMAX_PLAYER_STRATEGY_HPP
#define MINIMAX_PLAYER_STRATEGY_HPP

#pragma once

#include "PlayerStrategy.hpp"
#include "Board.hpp"

class MinimaxPlayerStrategy : public virtual PlayerStrategy {

public:

    MinimaxPlayerStrategy(int maxDepth);

    std::optional<Position> computeMove(Board &board) override;

private:

    struct Node;

    MinimaxPlayerStrategy::Node maximizingNode(Board &board, PlayerColor color, PlayerColor oppositeColor, int remainingDepth, int alpha, int beta);
    MinimaxPlayerStrategy::Node minimizingNode(Board &board, PlayerColor color, PlayerColor oppositeColor, int remainingDepth, int alpha, int beta);
    MinimaxPlayerStrategy::Node leafNode(Board &board, PlayerColor color, PlayerColor oppositeColor);

    int computeHeuristic(Board &board, PlayerColor color, PlayerColor oppositeColor);

    int maxDepth;

};

#endif
