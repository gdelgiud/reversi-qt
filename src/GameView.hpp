#ifndef GAME_VIEW_HPP
#define GAME_VIEW_HPP

#pragma once

#include <string>

class GameView {

public:

    virtual ~GameView() {
        // no-op
    }

    virtual void clearTile(int column, int row) = 0;
    virtual void showDisc(PlayerColor color, int column, int row) = 0;
    virtual void showAvailableMove(int column, int row) = 0;
    virtual void setLightPlayerHighlighted(bool highlighted) = 0;
    virtual void setDarkPlayerHighlighted(bool highlighted) = 0;
    virtual void setLightPlayerName(std::string name) = 0;
    virtual void setDarkPlayerName(std::string name) = 0;
    virtual void setLightPlayerScore(int score) = 0;
    virtual void setDarkPlayerScore(int score) = 0;
    virtual void showGameOverMessage(std::string winnerName) = 0;
    virtual void showGameDrawMessage() = 0;
    virtual void setHintButtonEnabled(bool enabled) = 0;
    virtual void setUndoButtonEnabled(bool enabled) = 0;
    virtual void showHint(PlayerColor color, int column, int row) = 0;
    virtual void stopHint() = 0;
    virtual void animateDiscFlip(int column, int row, PlayerColor from, PlayerColor to, long timeOffsetMillis) = 0;
    virtual void draw() = 0;

};

#endif
