#pragma once

#include <stack>
#include <QFuture>
#include <QFutureWatcher>

#include "Board.hpp"
#include "Player.hpp"
#include "GameView.hpp"
#include "PlayerStrategy.hpp"

class GamePresenter {

public:

    GamePresenter(GameView *view, Board board, Player *lightPlayer, Player *darkPlayer);
    ~GamePresenter() noexcept;

    void newGame(Board &board, Player *lightPlayer, Player *darkPlayer);
    void onViewStarted();
    void onViewReady();
    void onTileClicked(int column, int row);
    void onHintButtonClicked();
    void onUndoButtonClicked();

private:

    std::stack<Board> boardHistory;
    Player *lightPlayer;
    Player *darkPlayer;
    PlayerStrategy *hintStrategy;

    GameView *view;

    QFuture<std::optional<Position>> future;
    QFutureWatcher<std::optional<Position>> watcher;

    Board getBoard();
    Player *getCurrentPlayer();
    void onBoardChanged();
    void processNextMove();
    void applyMove(Position position);
    void applyPass();
    void onAiMoveFinished();
    void getDiscsToAnimate(Board &oldBoard, Board &newBoard, Position playedPosition);

};
