#include "Player.hpp"

Player::Player(std::string name, PlayerColor color, PlayerStrategy *strategy)
{
    this->name = name;
    this->color = color;
    this->strategy = strategy;
}

std::string Player::getName()
{
    return name;
}

PlayerStrategy *Player::getStrategy()
{
    return strategy;
}

