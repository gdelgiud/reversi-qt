#include "NewGameDialog.hpp"

#include <QRandomGenerator>

#include "MinimaxPlayerStrategy.hpp"

NewGameDialog::NewGameDialog(QWidget *parent) : QDialog(parent)
{
    mainLayout = new QVBoxLayout(this);

    playerConfigLayout = new QHBoxLayout(this);
    lightPlayerConfig = buildLightPlayerConfig();
    darkPlayerConfig = buildDarkPlayerConfig();
    playerConfigLayout->addWidget(lightPlayerConfig, 1);
    playerConfigLayout->addWidget(darkPlayerConfig, 1);
    mainLayout->addLayout(playerConfigLayout);

    startGameButton = new QPushButton("Start game");
    mainLayout->addWidget(startGameButton);
    startGameButton->setDefault(true);
    startGameButton->setFocus();

    setLayout(mainLayout);

    connect(startGameButton, &QPushButton::clicked, this, &QDialog::accept);
}

NewGameDialog::~NewGameDialog() noexcept
{
    delete mainLayout;
    delete playerConfigLayout;
    delete lightPlayerConfig;
    delete darkPlayerConfig;
    startGameButton->disconnect();
    delete startGameButton;
}

NewGameConfig NewGameDialog::getConfig()
{
    return NewGameConfig(getPlayerName(lightPlayerConfig),
                         getPlayerStrategy(lightPlayerConfig),
                         getPlayerName(darkPlayerConfig),
                         getPlayerStrategy(darkPlayerConfig));
}

PlayerConfigView *NewGameDialog::buildLightPlayerConfig()
{
    auto result = new PlayerConfigView("Light Player", this);
    result->setHumanPlayerChecked();
    result->setPlayerName(qgetenv("USER"));
    result->setAiDifficulty(1);
    return result;
}

PlayerConfigView *NewGameDialog::buildDarkPlayerConfig()
{
    auto result = new PlayerConfigView("Dark Player", this);
    result->setAiPlayerChecked();
    result->setAiDifficulty(1);
    return result;
}

PlayerStrategy *NewGameDialog::getPlayerStrategy(PlayerConfigView *config)
{
    if (config->isHumanSelected()) {
        return nullptr;
    }

    int aiMaxDepth;
    switch (config->getAiDifficulty()) {
        case 0:
            aiMaxDepth = 2;
            break;
        case 1:
            aiMaxDepth = 4;
            break;
        case 2:
            aiMaxDepth = 6;
            break;
        default:
            aiMaxDepth = 4;
            break;
    }
    return new MinimaxPlayerStrategy(aiMaxDepth);
}

std::string NewGameDialog::getPlayerName(PlayerConfigView *config)
{
    std::string name = config->getPlayerName().trimmed().toStdString();
    if (name.empty() || !config->isHumanSelected()) {
        name = getRandomName();
    }
    return name;
}

std::string NewGameDialog::getRandomName()
{
    std::string names[] = { "John Carmack", "Bjarne Stroustrup", "GLaDOS", "10001011101", "Linus Torvalds", "Stack Overflow",
            "C++", "Lvl 23 Java V.M.", "Roger Waters", "Red-Black Tree", "0x3A28213F", "That Guy", "Divide by Zero", "Humans Are Weak", "Wolfgang A. Mozart" };
    int size = std::extent<decltype(names)>::value;
    int random = QRandomGenerator::global()->bounded(size);
    return names[random];
}
