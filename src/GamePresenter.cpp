#include "GamePresenter.hpp"

#include <set>
#include <QTimer>
#include <iostream>
#include <QtConcurrent/QtConcurrentRun>

#include "MinimaxPlayerStrategy.hpp"

GamePresenter::GamePresenter(GameView *view, Board board, Player *lightPlayer, Player *darkPlayer)
{
    this->view = view;
    this->lightPlayer = lightPlayer;
    this->darkPlayer = darkPlayer;
    boardHistory.push(board);
    hintStrategy = new MinimaxPlayerStrategy(1);
    QObject::connect(&watcher, &QFutureWatcher<std::optional<Position>>::finished, [=] { onAiMoveFinished(); });
}

GamePresenter::~GamePresenter() noexcept
{
    while(!boardHistory.empty()) {
        boardHistory.pop();
    }
    delete hintStrategy;
}

void GamePresenter::newGame(Board &board, Player *lightPlayer, Player *darkPlayer)
{
    this->lightPlayer = lightPlayer;
    this->darkPlayer = darkPlayer;
    while (!boardHistory.empty()) {
        boardHistory.pop();
    }
    boardHistory.push(board);
    onViewStarted();
}

void GamePresenter::onViewStarted()
{
    view->setLightPlayerName(lightPlayer->getName());
    view->setDarkPlayerName(darkPlayer->getName());
    onBoardChanged();
}

void GamePresenter::onViewReady() {
    Board board = getBoard();

    bool currentPlayerIsHuman = getCurrentPlayer()->getStrategy() == nullptr;
    bool bothPlayersAreAi = lightPlayer->getStrategy() == nullptr && darkPlayer->getStrategy() == nullptr;
    bool isFinished = !board.isEmpty() && board.isFinished();
    view->setUndoButtonEnabled(boardHistory.size() > 1 && (currentPlayerIsHuman || (isFinished && bothPlayersAreAi)));
    view->setHintButtonEnabled(!isFinished && currentPlayerIsHuman);

    if (!board.isFinished()) {
        processNextMove();

        if (currentPlayerIsHuman) {
            for (Position p : board.getAvailableMoves()) {
                view->showAvailableMove(p.getColumn(), p.getRow());
            }
        }
    }

    int lightPlayerScore = board.getScore(light);
    int darkPlayerScore = board.getScore(dark);
    PlayerColor currentPlayer = board.getCurrentPlayer();
    view->setLightPlayerHighlighted(!isFinished && currentPlayer == light);
    view->setDarkPlayerHighlighted(!isFinished && currentPlayer == dark);
    view->setLightPlayerScore(lightPlayerScore);
    view->setDarkPlayerScore(darkPlayerScore);
    if (isFinished) {
        if (lightPlayerScore > darkPlayerScore) {
            view->showGameOverMessage(lightPlayer->getName());
        } else if (darkPlayerScore > lightPlayerScore) {
            view->showGameOverMessage(darkPlayer->getName());
        } else {
            view->showGameDrawMessage();
        }
    }
}

void GamePresenter::onTileClicked(int column, int row)
{
    if (getCurrentPlayer()->getStrategy() == nullptr && !getBoard().isFinished()) {
        applyMove(Position(column, row));
    }
}

void GamePresenter::onHintButtonClicked() {
    Player *currentPlayer = getCurrentPlayer();
    PlayerStrategy *currentPlayerStrategy = currentPlayer->getStrategy();
    if (currentPlayerStrategy == nullptr) {
        Board board = getBoard();
        std::optional<Position> hint = hintStrategy->computeMove(board);
        if (hint.has_value()) {
            view->showHint(board.getCurrentPlayer(), hint.value().getColumn(), hint.value().getRow());
        } else {
            view->stopHint();
        }
    }
}

void GamePresenter::onUndoButtonClicked()
{
    if (boardHistory.size() > 1) {
        do {
            boardHistory.pop();
        } while (boardHistory.size() > 1 && (getCurrentPlayer()->getStrategy() != nullptr || getBoard().getAvailableMoves().empty()));
        view->stopHint();
        onBoardChanged();
    }
}

Board GamePresenter::getBoard()
{
    return boardHistory.top();
}

Player *GamePresenter::getCurrentPlayer()
{
    if (getBoard().getCurrentPlayer() == light) {
        return lightPlayer;
    } else {
        return darkPlayer;
    }
}


void GamePresenter::processNextMove()
{
    Board currentBoard = getBoard();

    // Pass turns automatically
    std::set<Position> availableMoves = currentBoard.getAvailableMoves();
    if (availableMoves.empty()) {
        applyPass();
    } else {
        // Run whatever strategy the player has set, or wait for the user to make a move if it doesn't
        Player *currentPlayer = getCurrentPlayer();
        PlayerStrategy *currentPlayerStrategy = currentPlayer->getStrategy();
        if (currentPlayerStrategy != nullptr) {
            future = QtConcurrent::run(QThreadPool::globalInstance(), currentPlayerStrategy, &PlayerStrategy::computeMove, currentBoard);
            watcher.setFuture(future);
        }
    }
}

void GamePresenter::onAiMoveFinished()
{
    std::optional<Position> move = future.result();
    if (move.has_value()) {
        applyMove(move.value());
    }
}

void GamePresenter::applyMove(Position position)
{
    view->stopHint();
    Board oldBoard = getBoard();
    std::optional<Board> newBoardOptional = oldBoard.applyMove(position);
    if (newBoardOptional.has_value()) {
        Board newBoard = newBoardOptional.value();
        boardHistory.push(newBoard);
        onBoardChanged();

        PlayerColor playedColor = newBoard.getTile(position).getColor();
        PlayerColor oppositeColor = oppositeOf(playedColor);
        Position (Position::*directions[])() = {
            &Position::up,
            &Position::down,
            &Position::left,
            &Position::right,
            &Position::upLeft,
            &Position::upRight,
            &Position::downLeft,
            &Position::downRight
        };
        std::vector<Position> tilesToAnimate;
        for (int i = 0; i < 8; i++) {
            auto direction = directions[i];
            for (Position p = (position.*direction)();; p = (p.*direction)()) {
                Tile oldTile = oldBoard.getTile(p);
                Tile newTile = newBoard.getTile(p);
                if (oldTile.isEmpty() == newTile.isEmpty() && oldTile.getColor() == newTile.getColor()) {
                    break;
                }
                tilesToAnimate.push_back(p);
            }
        }
        for (size_t i = 0; i < tilesToAnimate.size(); i++) {
            Position p = tilesToAnimate[i];
            view->animateDiscFlip(p.getColumn(), p.getRow(), oppositeColor, playedColor, i * 200 + 200);
        }
    }

}

void GamePresenter::applyPass()
{
    view->stopHint();
    Board newBoard = getBoard().pass();
    boardHistory.push(newBoard);
    onBoardChanged();
}

void GamePresenter::onBoardChanged() {
    Board board = getBoard();

    for (int i = 0; i < 64; i++) {
        int column = i % 8;
        int row = i / 8;
        Position position = Position(column, row);
        Tile tile = board.getTile(position);
        if (tile.isEmpty()) {
            view->clearTile(column, row);
        } else {
            view->showDisc(tile.getColor(), column, row);
        }
    }
    view->draw();
}
