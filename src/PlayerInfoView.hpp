#pragma once

#include <QWidget>
#include <QLabel>
#include <QPaintEvent>

#include "DiscView.hpp"

class PlayerInfoView : public QWidget {

    Q_OBJECT

public:

    PlayerInfoView(PlayerColor color, QWidget *parent = 0);
    ~PlayerInfoView();

    void setScore(int score);
    void setName(QString name);
    void setHighlighted(bool highlighted);

protected:

    void paintEvent(QPaintEvent *e) override;

private:

    QLabel *score;
    QLabel *name;
    DiscView *discImage;
    bool highlighted = false;

};
