#include "RandomMovePlayerStrategy.hpp"

#include <cstdlib>
#include <set>

std::optional<Position> RandomMovePlayerStrategy::computeMove(Board &board)
{
    std::set<Position> availableMoves = board.getAvailableMoves();
    int randomIndex = std::rand() % availableMoves.size();
    int i = 0;
    for (auto position : availableMoves) {
        if (i == randomIndex) {
            return position;
        }
    }
    return { };
}
