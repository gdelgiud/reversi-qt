#include <vector>
#include <algorithm>
#include <cstring>

#include "Board.hpp"

const int BOARD_SIZE = 8;

Board::Board() {
    // no-op
}

Board::Board(Tile *tiles, PlayerColor currentPlayer)
{
    std::memcpy(&this->tiles, tiles, sizeof(Tile) * 64);
    this->currentPlayer = currentPlayer;
}

Board Board::emptyBoard() {
    return Board();
}

Board Board::defaultBoard()
{
    Board board = Board();
    board.setTile(Position(3, 3), light);
    board.setTile(Position(4, 3), dark);
    board.setTile(Position(4, 4), light);
    board.setTile(Position(3, 4), dark);
    return board;
}

static int toBoardIndex(Position position) {
    return position.getRow() * BOARD_SIZE + position.getColumn();
}

Tile Board::getTile(Position position)
{
    if (position.getColumn() < 0 || position.getColumn() >= BOARD_SIZE || position.getRow() < 0 || position.getRow() >= BOARD_SIZE) {
        return Tile::empty();
    }
    return tiles[toBoardIndex(position)];
}

void Board::setTile(Position position, PlayerColor color)
{
    tiles[toBoardIndex(position)] = Tile::with(color);
}

static void appendConvertibleCellsInDirection(Position (Position::*directionFunction)(), Position fromPosition, Board *board, PlayerColor color, std::vector<Position> *target) {
    Position buffer[8];
    int currentBufferIndex = 0;
    for (Position p = (fromPosition.*directionFunction)(); board->isValidPosition(p); p = (p.*directionFunction)()) {
        Tile tile = board->getTile(p);
        if (tile.isEmpty()) {
            break;
        } else if (tile.getColor() == color) {
            target->insert(target->begin(), std::begin(buffer), &buffer[currentBufferIndex]);
            break;
        } else {
            buffer[currentBufferIndex++] = p;
        }
    }
}

static std::vector<Position> getConvertibleCellsFrom(Position position, Board *board, PlayerColor color) {
    std::vector<Position> result = std::vector<Position>();
    appendConvertibleCellsInDirection(&Position::up, position, board, color, &result);
    appendConvertibleCellsInDirection(&Position::down, position, board, color, &result);
    appendConvertibleCellsInDirection(&Position::left, position, board, color, &result);
    appendConvertibleCellsInDirection(&Position::right, position, board, color, &result);
    appendConvertibleCellsInDirection(&Position::upRight, position, board, color, &result);
    appendConvertibleCellsInDirection(&Position::upLeft, position, board, color, &result);
    appendConvertibleCellsInDirection(&Position::downRight, position, board, color, &result);
    appendConvertibleCellsInDirection(&Position::downLeft, position, board, color, &result);
    return result;
}

std::optional<Board> Board::applyMove(Position position)
{
    if (!getTile(position).isEmpty()) {
        return { };
    }

    std::vector<Position> convertibleCells = getConvertibleCellsFrom(position, this, currentPlayer);
    if (convertibleCells.empty()) {
        return { };
    }

    Board result = Board(tiles, oppositeOf(currentPlayer));

    for (unsigned int i = 0; i < convertibleCells.size(); i++) {
        result.setTile(convertibleCells[i], currentPlayer);
    }
    result.setTile(position, currentPlayer);

    return result;
}

bool Board::isValidPosition(Position position)
{
    return position.getRow() >= 0 && position.getRow() < BOARD_SIZE
        && position.getColumn() >= 0 && position.getColumn() < BOARD_SIZE;
}

std::set<Position> Board::getAvailableMoves()
{
    return getAvailableMoves(currentPlayer);
}

std::set<Position> Board::getAvailableMoves(PlayerColor color)
{
    std::set<Position> result = std::set<Position>();
    for (int i = 0; i < BOARD_SIZE * BOARD_SIZE; i++) {
        int column = i % BOARD_SIZE;
        int row = i / BOARD_SIZE;
        Position position = Position(column, row);
        if (getTile(position).isEmpty() && !getConvertibleCellsFrom(position, this, color).empty()) {
            result.insert(position);
        }
    }
    return result;
}

PlayerColor Board::getCurrentPlayer()
{
    return currentPlayer;
}

int Board::getScore(PlayerColor color)
{
    int count = 0;
    for (int i = 0; i < BOARD_SIZE * BOARD_SIZE; i++) {
        Tile tile = tiles[i];
        if (!tile.isEmpty() && tile.getColor() == color) {
            count++;
        }
    }
    return count;
}

Board Board::pass()
{
    return Board(tiles, oppositeOf(currentPlayer));
}

bool Board::isEmpty() {
    for (int i = 0; i < BOARD_SIZE * BOARD_SIZE; i++) {
        if (!tiles[i].isEmpty()) {
            return false;
        }
    }
    return true;
}

bool Board::isFinished()
{
    return getAvailableMoves(currentPlayer).empty()
    && getAvailableMoves(oppositeOf(currentPlayer)).empty();
}
