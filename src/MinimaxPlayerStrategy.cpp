#include "MinimaxPlayerStrategy.hpp"

#include <climits>

#include <set>

static const int tileWeights[] =
    {75,-1,1,1,1,1,-1,75,
    -1,-5,2,2,2,2,-5,-1,
    1,2,4,4,4,4,2,1,
    1,2,4,5,5,4,2,1,
    1,2,4,5,5,4,2,1,
    1,2,4,4,4,4,2,1,
    -1,-5,2,2,2,2,-5,-1,
    75,-1,1,1,1,1,-1,75};


struct MinimaxPlayerStrategy::Node {
    std::optional<Position> move;
    int evaluation;
};

MinimaxPlayerStrategy::MinimaxPlayerStrategy(int maxDepth)
{
    this->maxDepth = maxDepth;
}

std::optional<Position> MinimaxPlayerStrategy::computeMove(Board &board)
{
    return maximizingNode(board, board.getCurrentPlayer(), oppositeOf(board.getCurrentPlayer()), maxDepth, INT_MIN, INT_MAX).move;
}

MinimaxPlayerStrategy::Node MinimaxPlayerStrategy::maximizingNode(Board &board, PlayerColor color, PlayerColor oppositeColor, int remainingDepth, int alpha, int beta)
{
    if (board.isFinished() || remainingDepth == 0) {
        return leafNode(board, color, oppositeColor);
    }

    std::set<Position> availableMoves = board.getAvailableMoves();
    int maxEvaluation = INT_MIN;
    if (availableMoves.empty()) {
        Board resultingBoard = board.pass();
        int evaluation = minimizingNode(resultingBoard, color, oppositeColor, remainingDepth - 1, alpha, beta).evaluation;
        return { { }, evaluation };
    }

    Position bestMove;
    for (const Position position : availableMoves) {
        Board resultingBoard = board.applyMove(position).value();
        int evaluation = minimizingNode(resultingBoard, color, oppositeColor, remainingDepth - 1, alpha, beta).evaluation;
        if (evaluation >= maxEvaluation) {
            maxEvaluation = evaluation;
            bestMove = position;
        }
        alpha = std::max(alpha, maxEvaluation);
        if (beta <= alpha) {
            break;
        }
    }
    return { bestMove, maxEvaluation };
}

MinimaxPlayerStrategy::Node MinimaxPlayerStrategy::minimizingNode(Board &board, PlayerColor color, PlayerColor oppositeColor, int remainingDepth, int alpha, int beta)
{
    if (board.isFinished() || remainingDepth == 0) {
        return leafNode(board, color, oppositeColor);
    }

    std::set<Position> availableMoves = board.getAvailableMoves();
    int minEvaluation = INT_MAX;
    if (availableMoves.empty()) {
        Board resultingBoard = board.pass();
        int evaluation = maximizingNode(resultingBoard, color, oppositeColor, remainingDepth - 1, alpha, beta).evaluation;
        return { { }, evaluation };
    }

    Position bestMove;
    for (const Position position : availableMoves) {
        Board resultingBoard = board.applyMove(position).value();
        int evaluation = maximizingNode(resultingBoard, color, oppositeColor, remainingDepth - 1, alpha, beta).evaluation;
        if (evaluation <= minEvaluation) {
            minEvaluation = evaluation;
            bestMove = position;
        }
        beta = std::min(beta, minEvaluation);
        if (beta <= alpha) {
            break;
        }
    }
    return { bestMove , minEvaluation };
}

MinimaxPlayerStrategy::Node MinimaxPlayerStrategy::leafNode(Board &board, PlayerColor color, PlayerColor oppositeColor)
{
    Node node = Node();
    node.evaluation = computeHeuristic(board, color, oppositeColor);
    return node;
}

int MinimaxPlayerStrategy::computeHeuristic(Board &board, PlayerColor color, PlayerColor oppositeColor)
{
    int playerScore = board.getScore(color);
    int opponentScore = board.getScore(oppositeColor);

    // If the game is over, avoid nodes where the AI lost or drew the game
    if (board.isFinished()) {
        if (playerScore > opponentScore) {
            return INT_MAX;
        } else {
            return INT_MIN;
        }
    }

    // Compute the total stability for each player
    // Stability is the amount of tiles each player has
    // taking into account the positions they control.
    // For example, controlling the corners is more valuable
    // than controlling the center.
    int playerStability = 0;
    int opponentStability = 0;
    for (int i = 0; i < 8; i++) {
        for (int j = 0; j < 8; j++) {
            Tile tile = board.getTile(Position(j, i));
            if (!tile.isEmpty()) {
                int tileWeight = tileWeights[j * 8 + i];
                if (tile.getColor() == color) {
                    playerStability += tileWeight;
                } else {
                    opponentStability += tileWeight;
                }
            }
        }
    }

    int playerAvailableMoves = board.getAvailableMoves(color).size();
    int opponentAvailableMoves = board.getAvailableMoves(oppositeColor).size();

    // Set some weights for each relevant attribute
    // The aim is to prioritize critical positions as soon as possible, reduce opponent's options
    // in the mid-game, and begin to convert as many disks as possible in the end-game
    int remainingTiles = 64 - playerScore - opponentScore;
    int stabilityWeight = 0;
    int availableMovesWeight = 0;
    int scoreWeight = 0;
    if (remainingTiles > 30) {
        stabilityWeight = 5;
        availableMovesWeight = 3;
        scoreWeight = -20;
    } else if (remainingTiles > 12) {
        stabilityWeight = 3;
        availableMovesWeight = 3;
        scoreWeight = -1;
    } else {
        stabilityWeight = 1;
        availableMovesWeight = 1;
        scoreWeight = 4;
    }

    return stabilityWeight * (playerStability - opponentStability) +
    availableMovesWeight * (playerAvailableMoves - opponentAvailableMoves) +
    scoreWeight * (playerScore - opponentScore);
}
