#pragma once

#include <QWidget>

class AvailableMoveView : public QWidget {

    Q_OBJECT

public:

    AvailableMoveView(QWidget *parent = 0);

protected:

    void paintEvent(QPaintEvent *e) override;

};
