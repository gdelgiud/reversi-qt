#pragma once

enum PlayerColor { light = 0, dark = 1 };

PlayerColor oppositeOf(PlayerColor playerColor);
