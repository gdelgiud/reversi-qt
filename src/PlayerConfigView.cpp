#include "PlayerConfigView.hpp"

#include <QGroupBox>
#include <QLabel>
#include <QVBoxLayout>
#include <QLineEdit>
#include <QRadioButton>
#include <QComboBox>

PlayerConfigView::PlayerConfigView(const QString &title, QWidget *parent) : QGroupBox(title, parent)
{
    layout = new QVBoxLayout();
    setLayout(layout);

    humanRadioButton = new QRadioButton("Human");
    aiRadioButton = new QRadioButton("Computer");

    layout->addStretch(1);

    layout->addWidget(humanRadioButton);
    playerNameLabel = new QLabel("Name");
    playerNameEdit = new QLineEdit();
    layout->addWidget(playerNameLabel);
    layout->addWidget(playerNameEdit);

    layout->addWidget(aiRadioButton);

    aiDifficultyLabel = new QLabel("Difficulty");
    aiDifficultyCombo = new QComboBox();
    aiDifficultyCombo->addItem("Easy");
    aiDifficultyCombo->addItem("Normal");
    aiDifficultyCombo->addItem("Hard");
    layout->addWidget(aiDifficultyLabel);
    layout->addWidget(aiDifficultyCombo);

    layout->addStretch(1);

    connect(humanRadioButton, &QRadioButton::toggled, this, &PlayerConfigView::onPlayerTypeToggled);
    connect(aiRadioButton, &QRadioButton::toggled, this, &PlayerConfigView::onPlayerTypeToggled);
}

PlayerConfigView::~PlayerConfigView() noexcept
{
    delete humanRadioButton;
    delete playerNameLabel;
    delete playerNameEdit;

    delete aiRadioButton;
    delete aiDifficultyLabel;
    delete aiDifficultyCombo;
}

void PlayerConfigView::setHumanPlayerChecked()
{
    humanRadioButton->setChecked(true);
}

void PlayerConfigView::setAiPlayerChecked()
{
    aiRadioButton->setChecked(true);
}

void PlayerConfigView::setPlayerName(const QString &name)
{
    playerNameEdit->setText(name);
}

void PlayerConfigView::setAiDifficulty(int difficulty)
{
    aiDifficultyCombo->setCurrentIndex(difficulty);
}

void PlayerConfigView::onPlayerTypeToggled()
{
    playerNameLabel->setEnabled(humanRadioButton->isChecked());
    playerNameEdit->setEnabled(humanRadioButton->isChecked());

    aiDifficultyLabel->setEnabled(aiRadioButton->isChecked());
    aiDifficultyCombo->setEnabled(aiRadioButton->isChecked());
}

bool PlayerConfigView::isHumanSelected()
{
    return humanRadioButton->isChecked();
}

QString PlayerConfigView::getPlayerName()
{
    return playerNameEdit->text();
}

int PlayerConfigView::getAiDifficulty()
{
    return aiDifficultyCombo->currentIndex();
}

