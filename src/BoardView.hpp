#ifndef BOARD_VIEW_HPP
#define BOARD_VIEW_HPP

#pragma once

#include <QWidget>
#include <QGridLayout>
#include <QRubberBand>

#include "PlayerColor.hpp"

class BoardView : public QWidget {

    Q_OBJECT

public:

    BoardView(QWidget *parent = 0);
    ~BoardView() noexcept;

    void clearTile(int column, int row);
    void showDisc(PlayerColor color, int column, int row);
    void showAvailableMove(int column, int row);

signals:

    void tileClicked(int column, int row);

protected:

    void paintEvent(QPaintEvent *e) override;
    void mousePressEvent(QMouseEvent *e) override;
    bool event(QEvent *event) override;
    QSize minimumSizeHint() const override;
    QSize sizeHint() const override;
    void resizeEvent(QResizeEvent *e) override;

private:

    QGridLayout *layout;

    QRubberBand *hoverRubberBand;

    void doPainting();
    float getBoardStrokeWidth();
    float getTileSize();
    int getColumnAt(QPointF point);
    int getRowAt(QPointF point);
    void onHoverMove(QPointF point);
    void onHoverLeave();
};

#endif
