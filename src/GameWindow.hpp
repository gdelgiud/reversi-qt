#ifndef GAME_WINDOW_HPP
#define GAME_WINDOW_HPP

#pragma once

#include <QMainWindow>
#include <QLabel>
#include <QPushButton>

#include "GamePresenter.hpp"
#include "GameView.hpp"
#include "BoardView.hpp"
#include "PlayerInfoView.hpp"
#include "NewGameConfig.hpp"

class GameWindow : public QMainWindow, public virtual GameView {

    Q_OBJECT

public:

    GameWindow(Board board, Player *lightPlayer, Player *darkPlayer, QWidget *parent = 0);
    GameWindow(QWidget *parent = 0);
    ~GameWindow() noexcept;

    void newGame(Board &board, Player *lightPlayer, Player *darkPlayer);

    void clearTile(int column, int row) override;
    void showDisc(PlayerColor color, int column, int row) override;
    void showAvailableMove(int column, int row) override;
    void setLightPlayerHighlighted(bool highlighted) override;
    void setDarkPlayerHighlighted(bool highlighted) override;
    void setLightPlayerName(std::string name) override;
    void setDarkPlayerName(std::string name) override;
    void setLightPlayerScore(int score) override;
    void setDarkPlayerScore(int score) override;
    void showGameOverMessage(std::string winnerName) override;
    void showGameDrawMessage() override;
    void setHintButtonEnabled(bool enabled) override;
    void setUndoButtonEnabled(bool enabled) override;
    void showHint(PlayerColor color, int column, int row) override;
    void stopHint() override;
    void animateDiscFlip(int column, int row, PlayerColor from, PlayerColor to, long timeOffsetMillis) override;
    void draw() override;

public slots:

    void onNewGameButtonClicked();
    void onTileClicked(int column, int row);
    void onHintButtonClicked();
    void onUndoButtonClicked();
    void onNewGameDialogAccepted(NewGameConfig &newGameConfig);
    void onNewGameDialogRejected();

private:

    GamePresenter *presenter;
    BoardView *boardView;
    PlayerInfoView *lightPlayerInfo;
    PlayerInfoView *darkPlayerInfo;
    QPushButton *hintButton;
    QPushButton *undoButton;
    QTimer *hintTimer;
    int hintPulse = 0;
    int hintColumn;
    int hintRow;
    PlayerColor latestHintColor;
    bool firstShow = false;

    int remainingDiscsToAnimate = 0;

    void showEvent(QShowEvent *event) override;
    void createMenubar();
    void showNewGameDialog();

};

#endif
