#include "BoardView.hpp"

#include <QPainter>
#include <QMouseEvent>

#include "DiscView.hpp"
#include "TileView.hpp"

#include <iostream>

const int BOARD_SIZE = 8;
const int MINIMUM_TILE_SIZE = 32;
const int PREFERRED_TILE_SIZE = 64;

static TileView *getTileViewAt(QGridLayout *layout, int column, int row) {
    int index = row * BOARD_SIZE + column;
    return static_cast<TileView*>(layout->itemAt(index)->widget());
}

BoardView::BoardView(QWidget *parent) : QWidget(parent)
{
    layout = new QGridLayout(this);
    layout->setSpacing(0);
    layout->setMargin(0);
    setLayout(layout);
    for (int i = 0; i < BOARD_SIZE * BOARD_SIZE; i++) {
        int column = i % BOARD_SIZE;
        int row = i / BOARD_SIZE;
        QWidget *tile = new TileView(this);
        layout->addWidget(tile, row, column);
    }

    hoverRubberBand = new QRubberBand(QRubberBand::Rectangle, this);
    this->setAttribute(Qt::WA_Hover, true);
}

BoardView::~BoardView() noexcept
{
    for (int i = 0; i < layout->children().size(); i++) {
        delete layout->itemAt(i);
    }
    delete layout;
}


void BoardView::paintEvent(QPaintEvent *e) {
    Q_UNUSED(e);
    doPainting();
}

void BoardView::mousePressEvent(QMouseEvent* e)
{
    int column = getColumnAt(e->pos());
    int row = getRowAt(e->pos());
    emit tileClicked(column, row);
}

bool BoardView::event(QEvent *event)
{
    switch (event->type()) {
        case QEvent::HoverMove:
            onHoverMove(static_cast<QHoverEvent*>(event)->posF());
            return true;
        case QEvent::HoverEnter:
            onHoverMove(static_cast<QHoverEvent*>(event)->posF());
            return true;
            break;
        case QEvent::HoverLeave:
            onHoverLeave();
            return true;
            break;
        default:
            return QWidget::event(event);
    }
}

void BoardView::onHoverMove(QPointF point)
{
    QMarginsF margins = contentsMargins();
    float tileSize = getTileSize();
    float penWidth = getBoardStrokeWidth();
    int column = getColumnAt(point);
    int row = getRowAt(point);
    float rubberBandSize = tileSize - penWidth;
    if (column >= 0 && column < BOARD_SIZE && row >= 0 && row < BOARD_SIZE) {
        hoverRubberBand->show();
        hoverRubberBand->setGeometry(column * tileSize + margins.left() + penWidth / 2.0f,
                                     row * tileSize + margins.top() + penWidth / 2.0f,
                                     rubberBandSize,
                                     rubberBandSize);
    } else {
        hoverRubberBand->hide();
    }
}

void BoardView::onHoverLeave()
{
    hoverRubberBand->hide();
}


void BoardView::doPainting() {
    QMarginsF margins = contentsMargins();
    float tileSize = getTileSize();
    QPainter painter(this);
    painter.setRenderHint(QPainter::Antialiasing);
    float penWidth = getBoardStrokeWidth();

    QPen pen = QPen("#1b5e20");
    pen.setWidthF(penWidth);

    painter.setPen(pen);
    QBrush tileBrushes[2] = { QBrush("#4caf50"), QBrush("#388e3c") };

    for (int i = 0; i < BOARD_SIZE * BOARD_SIZE; i++) {
        int column = i % BOARD_SIZE;
        int row = i / BOARD_SIZE;
        float x = (column * (tileSize)) + margins.left();
        float y = (row * (tileSize)) + margins.top();
        QBrush brush = tileBrushes[(row + column) % 2];
        QRectF tileRect = QRectF(x, y, tileSize, tileSize);
        painter.setBrush(brush);
        painter.drawRect(tileRect);
    }

    // Draw lines between tiles
    float xEnd = width() - margins.right() - penWidth / 2.0f;
    float yEnd = height() - margins.bottom() - penWidth / 2.0f;

    for (int i = 1; i < BOARD_SIZE; i++) {
        float xStart = i * tileSize + margins.left();
        float yStart = i * tileSize + margins.top();
        painter.drawLine(QPointF(xStart, margins.top() + penWidth / 2.0f), QPointF(xStart, yEnd));
        painter.drawLine(QPointF(margins.left() + penWidth / 2.0f, yStart), QPointF(xEnd, yStart));
    }

    // Draw outer border
    painter.setBrush(QBrush("#00000000"));
    painter.drawRect(QRectF(margins.left() + penWidth / 2.0f,
                     margins.top() + penWidth / 2.0f,
                     tileSize * BOARD_SIZE - penWidth,
                     tileSize * BOARD_SIZE - penWidth));
}

float BoardView::getTileSize()
{
    return std::min(width(), height()) / (float)BOARD_SIZE;
}

float BoardView::getBoardStrokeWidth()
{
    return getTileSize() * 0.04f;
}

int BoardView::getColumnAt(QPointF point)
{
    QMarginsF margins = contentsMargins();
    float x = point.x() - margins.left();
    float tileSize = getTileSize();
    return x / tileSize;
}

int BoardView::getRowAt(QPointF point)
{
    QMarginsF margins = contentsMargins();
    float y = point.y() - margins.top();
    float tileSize = getTileSize();
    return y / tileSize;
}

QSize BoardView::minimumSizeHint() const
{
    return QSize(BOARD_SIZE * MINIMUM_TILE_SIZE, BOARD_SIZE * MINIMUM_TILE_SIZE);
}

QSize BoardView::sizeHint() const
{
    return QSize(BOARD_SIZE * PREFERRED_TILE_SIZE, BOARD_SIZE * PREFERRED_TILE_SIZE);
}

void BoardView::resizeEvent(QResizeEvent *e)
{
    QSizeF fullSize = e->size();
    int targetSize = std::min(fullSize.width(), fullSize.height());
    int horizontalMargin = qRound((fullSize.width() - targetSize) / 2.0);
    int verticalMargin = qRound((fullSize.height() - targetSize) / 2.0);
    setContentsMargins(horizontalMargin, verticalMargin, horizontalMargin, verticalMargin);
}

void BoardView::clearTile(int column, int row)
{
    getTileViewAt(layout, column, row)->setEmpty();
}

void BoardView::showDisc(PlayerColor color, int column, int row)
{
    getTileViewAt(layout, column, row)->setColor(color);
}

void BoardView::showAvailableMove(int column, int row)
{
    getTileViewAt(layout, column, row)->setAvailableMove();
}
