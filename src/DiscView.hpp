#pragma once

#include <QWidget>

#include "PlayerColor.hpp"

class DiscView : public QWidget {

    Q_OBJECT

public:

    DiscView(PlayerColor color, QWidget *parent = 0);

    void setColor(PlayerColor color);

protected:

    void paintEvent(QPaintEvent *e) override;

private:

    PlayerColor color;

    void doPainting();

};
