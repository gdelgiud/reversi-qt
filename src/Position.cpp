#include "Position.hpp"

Position::Position() : Position(0, 0)
{
    // no-op
}

Position::Position(int column, int row)
{
    this->column = column;
    this->row = row;
}

Position Position::up()
{
    return Position(column, row - 1);
}

Position Position::down()
{
    return Position(column, row + 1);
}

Position Position::left()
{
    return Position(column - 1, row);
}

Position Position::right()
{
    return Position(column + 1, row);
}

Position Position::upRight()
{
    return Position(column + 1, row - 1);
}

Position Position::upLeft()
{
    return Position(column - 1, row - 1);
}

Position Position::downRight()
{
    return Position(column + 1, row + 1);
}

Position Position::downLeft()
{
    return Position(column - 1, row + 1);
}

int Position::getColumn() const
{
    return column;
}

int Position::getRow() const
{
    return row;
}
