#include <QApplication>
#include <QWidget>

#include <ctime>
#include <cstdlib>

#include "GameWindow.hpp"

int main(int argc, char *argv[]) {
    std::srand(std::time(NULL));

    QApplication app(argc, argv);

    GameWindow *gameWindow = new GameWindow();
    gameWindow->show();
    app.processEvents();
    return app.exec();
}
