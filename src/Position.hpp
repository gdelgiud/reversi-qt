#pragma once

class Position {

public:

    Position();
    Position(int column, int row);

    Position up();
    Position down();
    Position left();
    Position right();
    Position upRight();
    Position upLeft();
    Position downRight();
    Position downLeft();

    int getColumn() const;
    int getRow() const;

    bool operator<(Position const &p) const {
        return column < p.column || (column == p.column && row < p.row);
    }

private:

    int column;
    int row;

};
