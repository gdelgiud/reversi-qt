#ifndef NEW_GAME_WINDOW_HPP
#define NEW_GAME_WINDOW_HPP

#pragma once

#include <QWidget>
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QGroupBox>
#include <QPushButton>
#include <QDialog>

#include "PlayerConfigView.hpp"
#include "Player.hpp"
#include "PlayerStrategy.hpp"
#include "NewGameConfig.hpp"

class NewGameDialog : public QDialog {

    Q_OBJECT

public:

    NewGameDialog(QWidget *parent = 0);
    ~NewGameDialog() noexcept;

    NewGameConfig getConfig();

private:

    QVBoxLayout *mainLayout;
    QHBoxLayout *playerConfigLayout;
    PlayerConfigView *lightPlayerConfig;
    PlayerConfigView *darkPlayerConfig;

    QPushButton *startGameButton;

    PlayerConfigView *buildLightPlayerConfig();
    PlayerConfigView *buildDarkPlayerConfig();
    PlayerStrategy *getPlayerStrategy(PlayerConfigView *config);
    std::string getPlayerName(PlayerConfigView *config);
    std::string getRandomName();

};

#endif
