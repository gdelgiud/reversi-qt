#ifndef PLAYER_STRATEGY_HPP
#define PLAYER_STRATEGY_HPP

#pragma once

#include <optional>

#include "Position.hpp"
#include "Board.hpp"
#include "PlayerColor.hpp"

class PlayerStrategy {

public:

    virtual ~PlayerStrategy() noexcept { /* no-op */ };
    virtual std::optional<Position> computeMove(Board &board) = 0;

};

#endif
