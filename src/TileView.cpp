#include "TileView.hpp"

#include <QPainter>
#include <QGridLayout>
#include <QResizeEvent>

const float DISC_SIZE_PERCENTAGE = 0.8f;
const float AVAILABLE_MOVE_PERCENTAGE = 0.4f;

TileView::TileView(QWidget *parent) : QWidget(parent)
{
    auto *layout = new QGridLayout(this); // Whatever lets me nest two QWidgets inside
    layout->setSpacing(0);
    layout->setContentsMargins(QMargins());
    discView = new DiscView(light, this);
    availableMoveView = new AvailableMoveView(this);
    layout->addWidget(discView);
    layout->addWidget(availableMoveView);
}

TileView::~TileView() noexcept
{
    delete discView;
    delete availableMoveView;
}


void TileView::setEmpty()
{
    discView->hide();
    availableMoveView->hide();
}

void TileView::setColor(PlayerColor color)
{
    discView->show();
    discView->setColor(color);
    availableMoveView->hide();
}

void TileView::setAvailableMove()
{
    availableMoveView->show();
    discView->hide();
}

void TileView::resizeEvent(QResizeEvent *e)
{
    QSize fullSize = e->size();
    float newDiscSize = fullSize.width() * DISC_SIZE_PERCENTAGE;
    float newAvailableMoveSize = fullSize.width() * AVAILABLE_MOVE_PERCENTAGE;
    discView->setFixedSize(QSize(newDiscSize, newDiscSize));
    availableMoveView->setFixedSize(QSize(newAvailableMoveSize, newAvailableMoveSize));
}
