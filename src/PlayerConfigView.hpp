#ifndef PLAYER_CONFIG_HPP
#define PLAYER_CONFIG_HPP

#pragma once

#include <QGroupBox>
#include <QLabel>
#include <QVBoxLayout>
#include <QLineEdit>
#include <QRadioButton>
#include <QComboBox>

class PlayerConfigView : public QGroupBox {

    Q_OBJECT;

public:

    PlayerConfigView(const QString &title, QWidget *parent = 0);
    ~PlayerConfigView() noexcept;

    void setHumanPlayerChecked();
    void setAiPlayerChecked();
    void setPlayerName(const QString &name);
    void setAiDifficulty(int difficulty);

    bool isHumanSelected();
    QString getPlayerName();
    int getAiDifficulty();

public slots:

    void onPlayerTypeToggled();

private:

    QVBoxLayout *layout;

    QRadioButton *humanRadioButton;
    QLabel *playerNameLabel;
    QLineEdit *playerNameEdit;

    QRadioButton *aiRadioButton;
    QLabel *aiDifficultyLabel;
    QComboBox *aiDifficultyCombo;

};

#endif
