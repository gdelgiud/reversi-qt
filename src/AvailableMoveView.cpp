#include "AvailableMoveView.hpp"

#include <QPainter>

AvailableMoveView::AvailableMoveView(QWidget *parent) : QWidget(parent)
{
    // no-op
}

void AvailableMoveView::paintEvent(QPaintEvent *e)
{
    Q_UNUSED(e);
    QPainter painter(this);
    painter.setRenderHint(QPainter::Antialiasing);
    QBrush fillBrush = QBrush("#2196f3");
    QPen stroke = QPen("#44212121");
    float penWidth = width() * 0.08f;
    QRectF diskRect = QRectF(penWidth / 2, penWidth / 2, width() - penWidth, height() - penWidth);
    stroke.setWidthF(penWidth);
    painter.setBrush(fillBrush);
    painter.setPen(stroke);
    painter.drawEllipse(diskRect);
}
