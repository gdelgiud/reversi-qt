#ifndef RANDOM_PLAYER_STRATEGY
#define RANDOM_PLAYER_STRATEGY

#include "PlayerStrategy.hpp"

class RandomMovePlayerStrategy : public virtual PlayerStrategy {

public:

    std::optional<Position> computeMove(Board &board) override;

};

#endif
