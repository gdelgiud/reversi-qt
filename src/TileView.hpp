#pragma once

#include <QWidget>

#include "AvailableMoveView.hpp"
#include "DiscView.hpp"

class TileView : public QWidget {

    Q_OBJECT

public:

    TileView(QWidget *parent = 0);
    ~TileView() noexcept;

    void setEmpty();
    void setColor(PlayerColor color);
    void setAvailableMove();

protected:

    void resizeEvent(QResizeEvent *e) override;

private:

    DiscView *discView;
    AvailableMoveView *availableMoveView;
};
