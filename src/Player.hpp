#ifndef PLAYER_HPP
#define PLAYER_HPP

#pragma once

#include <string>

#include "PlayerStrategy.hpp"
#include "PlayerColor.hpp"

class Player {

public:

    Player(std::string name, PlayerColor color, PlayerStrategy *strategy = nullptr);

    std::string getName();
    PlayerStrategy *getStrategy();

private:

    std::string name;
    PlayerColor color;
    PlayerStrategy *strategy;

};

#endif
