#include "PlayerInfoView.hpp"

#include <QHBoxLayout>
#include <QPainter>
#include <QFont>
#include <QImage>

#include "DiscView.hpp"

PlayerInfoView::PlayerInfoView(PlayerColor color, QWidget *parent) : QWidget(parent)
{
    auto *layout = new QHBoxLayout(this);
    auto *textLayout = new QVBoxLayout();
    score = new QLabel(this);
    name = new QLabel(this);
    discImage = new DiscView(color);
    discImage->setFixedSize(QSize(64, 64));
    QFont scoreFont = score->font();
    scoreFont.setPointSize(24);
    score->setFont(scoreFont);
    QFont nameFont = name->font();
    nameFont.setPointSize(16);
    name->setFont(nameFont);
    textLayout->addWidget(name, 0, Qt::AlignLeft);
    textLayout->addWidget(score, 0, Qt::AlignLeft);
    textLayout->addStretch(1);

    layout->addWidget(discImage, 0, Qt::AlignCenter);
    layout->addLayout(textLayout, 1);
}

PlayerInfoView::~PlayerInfoView()
{
    delete score;
    delete name;
    delete discImage;
}

void PlayerInfoView::setScore(int score)
{
    this->score->setText(QString::number(score));
}

void PlayerInfoView::setName(QString name)
{
    this->name->setText(name);
}

void PlayerInfoView::setHighlighted(bool highlighted)
{
    this->highlighted = highlighted;
    update();
}

void PlayerInfoView::paintEvent(QPaintEvent *e)
{
    Q_UNUSED(e);
    if (highlighted) {
        QPainter painter(this);
        QBrush backgroundBrush = palette().brush(QPalette::Highlight);
        QPen stroke = QPen("#00000000");
        painter.setBrush(backgroundBrush);
        painter.setPen(stroke);
        painter.drawRect(0, 0, width(), height());
    }
}
