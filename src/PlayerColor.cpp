#include "PlayerColor.hpp"

PlayerColor oppositeOf(PlayerColor playerColor)
{
    return static_cast<PlayerColor>(1 - static_cast<int>(playerColor));
}
