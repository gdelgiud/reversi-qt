#include "GameWindow.hpp"

#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QSizePolicy>
#include <QMessageBox>
#include <QTimer>
#include <QAction>
#include <QMenu>
#include <QMenuBar>

#include "NewGameDialog.hpp"

GameWindow::GameWindow(Board board, Player *lightPlayer, Player *darkPlayer, QWidget *parent) : QMainWindow(parent)
{
    auto mainWidget = new QWidget(this);
    presenter = new GamePresenter(this, board, lightPlayer, darkPlayer);
    boardView = new BoardView();
    boardView->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
    lightPlayerInfo = new PlayerInfoView(light, this);
    darkPlayerInfo = new PlayerInfoView(dark, this);
    hintButton = new QPushButton("Hint", this);
    undoButton = new QPushButton("Undo", this);

    auto *scoreLayout = new QVBoxLayout();
    scoreLayout->addWidget(lightPlayerInfo, 0, Qt::AlignTop);
    scoreLayout->addWidget(darkPlayerInfo, 0, Qt::AlignTop);
    scoreLayout->addStretch(1);
    scoreLayout->addWidget(hintButton);
    scoreLayout->addWidget(undoButton);

    auto *mainLayout = new QHBoxLayout(this);
    mainLayout->addWidget(boardView, 75);
    mainLayout->addLayout(scoreLayout, 25);

    setCentralWidget(mainWidget);
    mainWidget->setLayout(mainLayout);

    connect(hintButton, &QPushButton::clicked, this, &GameWindow::onHintButtonClicked);
    connect(undoButton, &QPushButton::clicked, this, &GameWindow::onUndoButtonClicked);
    connect(boardView, &BoardView::tileClicked, this, &GameWindow::onTileClicked);

    hintTimer = new QTimer(this);
    connect(hintTimer, &QTimer::timeout, this, [=] {
        if (hintPulse % 2 == 0) {
            boardView->showAvailableMove(hintColumn, hintRow);
        } else {
            boardView->showDisc(latestHintColor, hintColumn, hintRow);
        }
        hintPulse++;
    });

    createMenubar();

    presenter->onViewStarted();
}

GameWindow::GameWindow(QWidget *parent) : GameWindow(Board::emptyBoard(), new Player("", PlayerColor::light), new Player("", PlayerColor::dark, nullptr), parent)
{
    // no-op
}

GameWindow::~GameWindow() noexcept
{
    delete presenter;
    delete lightPlayerInfo;
    delete darkPlayerInfo;
    delete boardView;
    delete hintTimer;
}

void GameWindow::createMenubar()
{
    auto newGame = new QAction("&New Game", this);
    newGame->setShortcut(QKeySequence::New);
    auto quit = new QAction("&Quit", this);
    quit->setShortcut(QKeySequence::Quit);
    auto hint = new QAction("&Hint", this);
    hint->setShortcut(QKeySequence(Qt::CTRL + Qt::Key_H));
    auto undo = new QAction("&Undo", this);
    undo->setShortcut(QKeySequence::Undo);

    auto file = menuBar()->addMenu("&Game");
    file->addAction(newGame);
    file->addSeparator();
    file->addAction(hint);
    file->addAction(undo);
    file->addSeparator();
    file->addAction(quit);

    connect(newGame, &QAction::triggered, this, &GameWindow::onNewGameButtonClicked);
    connect(hint, &QAction::triggered, this, &GameWindow::onHintButtonClicked);
    connect(undo, &QAction::triggered, this, &GameWindow::onUndoButtonClicked);
    connect(quit, &QAction::triggered, this, &GameWindow::close);
}


void GameWindow::newGame(Board &board, Player *lightPlayer, Player *darkPlayer)
{
    presenter->newGame(board, lightPlayer, darkPlayer);
}

void GameWindow::onHintButtonClicked()
{
    presenter->onHintButtonClicked();
}

void GameWindow::onUndoButtonClicked() {
    presenter->onUndoButtonClicked();
}

void GameWindow::onTileClicked(int column, int row)
{
    presenter->onTileClicked(column, row);
}

void GameWindow::onNewGameButtonClicked()
{
    showNewGameDialog();
}

void GameWindow::clearTile(int column, int row) {
    boardView->clearTile(column, row);
}

void GameWindow::showDisc(PlayerColor color, int column, int row)
{
    boardView->showDisc(color, column, row);
}

void GameWindow::showAvailableMove(int column, int row)
{
    boardView->showAvailableMove(column, row);
}

void GameWindow::setLightPlayerHighlighted(bool highlighted)
{
    lightPlayerInfo->setHighlighted(highlighted);
}

void GameWindow::setDarkPlayerHighlighted(bool highlighted)
{
    darkPlayerInfo->setHighlighted(highlighted);
}

void GameWindow::setLightPlayerName(std::string name)
{
    lightPlayerInfo->setName(QString::fromStdString(name));
}

void GameWindow::setDarkPlayerName(std::string name)
{
    darkPlayerInfo->setName(QString::fromStdString(name));
}

void GameWindow::setLightPlayerScore(int score)
{
    lightPlayerInfo->setScore(score);
}

void GameWindow::setDarkPlayerScore(int score)
{
    darkPlayerInfo->setScore(score);
}

void GameWindow::showGameOverMessage(std::string winnerName)
{
    QMessageBox msgBox;
    QString name = QString::fromStdString(winnerName);
    QString message = " has won";
    QString result;
    result.reserve(name.length() + message.length());
    result.append(name);
    result.append(message);
    msgBox.setWindowTitle("Game over -- Reversi");
    msgBox.setText(result);
    msgBox.setIcon(QMessageBox::Information);
    msgBox.exec();
}

void GameWindow::showGameDrawMessage()
{
    QMessageBox msgBox;
    msgBox.setWindowTitle("Game over -- Reversi");
    msgBox.setText("Players have drawn");
    msgBox.setIcon(QMessageBox::Information);
    msgBox.exec();
}

void GameWindow::setHintButtonEnabled(bool enabled)
{
    hintButton->setEnabled(enabled);
}

void GameWindow::setUndoButtonEnabled(bool enabled)
{
    undoButton->setEnabled(enabled);
}

void GameWindow::showHint(PlayerColor color, int column, int row)
{
    hintPulse = 0;
    hintColumn = column;
    hintRow = row;
    latestHintColor = color;
    boardView->showDisc(latestHintColor, column, row);
    hintTimer->start(500);
}

void GameWindow::stopHint()
{
    hintTimer->stop();
}

void GameWindow::animateDiscFlip(int column, int row, PlayerColor from, PlayerColor to, long timeOffsetMillis)
{
    remainingDiscsToAnimate++;
    showDisc(from, column, row);
    QTimer::singleShot(timeOffsetMillis, this, [=] {
        showDisc(to, column, row);
        remainingDiscsToAnimate--;
        if (remainingDiscsToAnimate == 0) {
            presenter->onViewReady();
        }
    });
}

void GameWindow::draw()
{
    QTimer::singleShot(10, this, [=] {
        if (remainingDiscsToAnimate == 0) {
            presenter->onViewReady();
        }
    });
}

void GameWindow::showEvent(QShowEvent *event)
{
    Q_UNUSED(event);
    if (!firstShow) {
        // Time offset needed to make sure the dialog is centered on this window
        QTimer::singleShot(100, this, &GameWindow::onNewGameButtonClicked);
    }
}

void GameWindow::showNewGameDialog()
{
    NewGameDialog *newGameDialog = new NewGameDialog(this);
    connect(newGameDialog, &QDialog::accepted, this, [this, newGameDialog]() {
        NewGameConfig newGameConfig = newGameDialog->getConfig();
        onNewGameDialogAccepted(newGameConfig);
    });
    connect(newGameDialog, &QDialog::rejected, this, &GameWindow::onNewGameDialogRejected);
    newGameDialog->open();
}

void GameWindow::onNewGameDialogAccepted(NewGameConfig &newGameConfig)
{
    firstShow = true;
    Board board = Board::defaultBoard();
    newGame(board, newGameConfig.getLightPlayer(), newGameConfig.getDarkPlayer());
}

void GameWindow::onNewGameDialogRejected()
{
    if (!firstShow) {
        close();
    }
}
