#include "DiscView.hpp"

#include <QPainter>
#include <QBrush>
#include <QRectF>

DiscView::DiscView(PlayerColor color, QWidget *parent) : QWidget(parent)
{
    setColor(color);
}

void DiscView::setColor(PlayerColor color)
{
    this->color = color;
    update();
}


void DiscView::paintEvent(QPaintEvent *e)
{
    Q_UNUSED(e)
    doPainting();
}

void DiscView::doPainting()
{
    QPainter painter(this);
    painter.setRenderHint(QPainter::Antialiasing);
    float penWidth = width() * 0.08f;
    QPen pen = QPen();
    pen.setWidthF(penWidth);
    pen.setColor("#44212121");
    painter.setPen(pen);
    QBrush brush = color == light? QBrush("#FEFEFE") : QBrush("#1A1A1A");
    float offset = penWidth / 2.0f;
    QRectF rect = QRectF(offset, offset, width() - penWidth, height() - penWidth);

    painter.setBrush(brush);
    painter.drawEllipse(rect);
}
