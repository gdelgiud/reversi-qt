#pragma once

#include "PlayerColor.hpp"

class Tile {
public:

    Tile();
    static Tile empty();
    static Tile with(PlayerColor playerColor);

    bool isEmpty();
    PlayerColor getColor();

private:

    Tile(PlayerColor playerColor);

    PlayerColor playerColor;
    bool is_empty;

};
