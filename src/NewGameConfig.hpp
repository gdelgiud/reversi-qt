#ifndef NEW_GAME_CONFIG_HPP
#define NEW_GAME_CONFIG_HPP

#include <string>

#include "PlayerStrategy.hpp"
#include "Player.hpp"

class NewGameConfig {

public:

    NewGameConfig(std::string lightPlayerName, PlayerStrategy *lightPlayerStrategy, std::string darkPlayerName, PlayerStrategy *darkPlayerStrategy);

    Player *getLightPlayer();
    Player *getDarkPlayer();

private:

    std::string lightPlayerName;
    PlayerStrategy *lightPlayerStrategy;

    std::string darkPlayerName;
    PlayerStrategy *darkPlayerStrategy;
};

#endif
