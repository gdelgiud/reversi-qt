#include "NewGameConfig.hpp"

NewGameConfig::NewGameConfig(std::string lightPlayerName, PlayerStrategy *lightPlayerStrategy, std::string darkPlayerName, PlayerStrategy *darkPlayerStrategy)
{
    this->lightPlayerName = lightPlayerName;
    this->lightPlayerStrategy = lightPlayerStrategy;
    this->darkPlayerName = darkPlayerName;
    this->darkPlayerStrategy = darkPlayerStrategy;
}

Player *NewGameConfig::getLightPlayer()
{
    return new Player(lightPlayerName, PlayerColor::light, lightPlayerStrategy);
}

Player *NewGameConfig::getDarkPlayer()
{
    return new Player(darkPlayerName, PlayerColor::dark, darkPlayerStrategy);
}
